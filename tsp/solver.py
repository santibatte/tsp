"""
"""


class Solver:
    """"""

    def __init__(self, algorithms):
        self.algorithms = algorithms
        self.solutions = {}

    #def plot_solution(path):
    #     plt.plot(cities[:,0], cities[:,1], 'co')
    #                plt.xlim(0, MAX_DISTANCE)
    #                        plt.ylim(0, MAX_DISTANCE)

    #                                for (_from, to) in pairwise(path):
    #                                            plt.arrow(cities[_from][0], cities[_from][1],
    #                                                                  cities[to][0]- cities[_from][0], cities[to][1] - cities[_from][1],
    #                                                                                    color='b', length_includes_head=True)

    def solve(self, tsp):
        print(f"Usaremos {len(self.algorithms)} algoritmos para resolver este problema.")
        for algorithm in self.algorithms:
            print(f"Solving TSP problem with {algorithm}")
            self.solutions[str(algorithm)] = algorithm.run(tsp)

        best_solution = min(self.solutions.items(), key=lambda x: x[1]["cost"])

        return best_solution
